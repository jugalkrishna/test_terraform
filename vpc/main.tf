data "aws_availability_zones" "available" {}

variable "cidr" {
  description = "The CIDR block for the VPC."
}
variable "business_unit" {
	description 	= "The name of your Business Unit"
}
variable "region" {
	description		= "The AWS region in which the resources are created"
}
variable "available_business_units" {
	type = "map"
	description 	= "The name of your Business Units"
	default = {
			"Custom House"	=	"h"
			"Vigo"			=	"v"		
			"Paymap"		=	"p"
			"Speedpay"		=	"s"
			"Orlandi Valuta"=	"o"
			"Western Union"	=	"w"
			"WUBS"			=	"b"
	}
}
variable "region_code" {
	type = "map"
	description 	= "The code for your region"
	default = {
			"us-east-1"			= "aev"
			"us-east-2"			= "aeo"
			"us-west-1"			= "awc"
			"us-west-2"			= "awo"
			"ca-central-1"		= "aca"
			"eu-central-1"		= "aef"
			"eu-west-1"			= "aei"
			"eu-west-2"			= "ael"
			"eu-west-3"			= "aep"
			"eu-north-1"		= "aes"
			"ap-northeast-1"	= "aat"
			"ap-northeast-2"	= "ak"
			"ap-northeast-3"	= "aao"
			"ap-southeast-1"	= "aap"
			"ap-southeast-2"	= "aas"
			"ap-south-1"		= "aam"
			"sa-east-1"			= "asp"

	}
}
variable "red_public_subnets" {
	description = "The list of CIDRs for Red Public Subnets"
	type = "list"
}

variable "yellow_app_subnets" {
	description = "The list of CIDRs for Yellow App Subnets"
	type = "list"
}

variable "green_db_subnets" {
	description = "The list of CIDRs for Green Db Subnets "
	type = "list"
}

variable "name" {
	description 	=	"The name of your stack"
	default = "stack"
}
resource "aws_vpc" "vpc" {
  cidr_block       		= "${var.cidr}"
  enable_dns_support 	= true
  enable_dns_hostnames 	= true

  tags = {
	Name		= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("vpc%03d",1)}"
	Customer 	= "Western Union"
	
  }
}
resource "aws_subnet" "red_public" {
  count       			= "${length(var.red_public_subnets)}"
  vpc_id				= "${aws_vpc.vpc.id}"
  cidr_block      		= "${element(var.red_public_subnets, count.index)}"
  availability_zone  	= "${data.aws_availability_zones.available.names[count.index]}"
  tags = {
	Name				= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("sub%03d", count.index+1)}"
	Customer 			= "Western Union"
	
  }
}
resource "aws_subnet" "yellow_app" {
  count       			= "${length(var.yellow_app_subnets)}"
  vpc_id				= "${aws_vpc.vpc.id}"
  cidr_block      		= "${element(var.yellow_app_subnets, count.index)}"
  availability_zone  	= "${data.aws_availability_zones.available.names[count.index]}"
  tags = {
	Name				= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("sub%03d", count.index+3)}"
	Customer 			= "Western Union"	
	
  }
}
resource "aws_subnet" "green_db" {
  count       			= "${length(var.green_db_subnets)}"
  vpc_id				= "${aws_vpc.vpc.id}"
  cidr_block      		= "${element(var.green_db_subnets, count.index)}"
  availability_zone  	= "${data.aws_availability_zones.available.names[count.index]}"
  tags = {
	Name				= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("sub%03d", count.index+5)}"
	Customer 			= "Western Union"
	
  }
}
resource "aws_vpc_dhcp_options" "dns_resolver" {
  domain_name = "ec2.internal"
  domain_name_servers = ["AmazonProvidedDNS"]
  tags = {
	Name				= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("dhc%03d", 1)}"
	Customer 			= "Western Union"
	
  }
}
resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  vpc_id          = "${aws_vpc.vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.dns_resolver.id}"
}
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
	Name				= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("igw%03d", 1)}"
	Customer 			= "Western Union"
  }
}
resource "aws_vpn_gateway" "vgw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
	Name				= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("vgw%03d", 1)}"
	Customer 			= "Western Union"
  }
}
resource "aws_route_table" "public1" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags = {
	Name				= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("rtb%03d", 1)}"
	Customer 			= "Western Union"
  }
}
resource "aws_route_table" "private1" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
	Name				= "wu${lookup(var.available_business_units,var.business_unit)}${lookup(var.region_code,var.region)}${format("rtb%03d", 2)}"
	Customer 			= "Western Union"
  }
}
resource "aws_route_table_association" "RedPublicSubnet" {
	count       = "${length(var.red_public_subnets)}"
	subnet_id	= "${element(aws_subnet.red_public.*.id,count.index)}"
	route_table_id	= "${aws_route_table.public1.id}"
}
resource "aws_route_table_association" "YellowAppSubnet" {
	count       = "${length(var.yellow_app_subnets)}"
	subnet_id	= "${element(aws_subnet.yellow_app.*.id,count.index)}"
	route_table_id	= "${aws_route_table.private1.id}"
}
resource "aws_route_table_association" "GreenDBSubnet" {
	count       = "${length(var.green_db_subnets)}"
	subnet_id	= "${element(aws_subnet.green_db.*.id,count.index)}"
	route_table_id	= "${aws_route_table.private1.id}"
}
