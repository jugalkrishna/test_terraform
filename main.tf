module "vpc" {
	source					= "./vpc"
	name					= "${var.name}"
	cidr					= "${var.cidr}"
	region					= "${var.region}"
	red_public_subnets		= "${var.red_public_subnets}"
	yellow_app_subnets		= "${var.yellow_app_subnets}"
	green_db_subnets		= "${var.green_db_subnets}"
	business_unit			= "${var.business_unit}"
	
}