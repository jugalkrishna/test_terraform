variable "name" {
	description 	= "The name of your stack"
}

variable "business_unit" {
	description 	= "The name of your Business Unit"
}

variable "region" {
	description		= "The AWS region in which the resources are created"
}

variable "cidr" {
	description = "The CIDR block for the VPC."
}

variable "red_public_subnets" {
	type = "list"
	description = "The list of CIDRs for Red Public Subnets"
}

variable "yellow_app_subnets" {
	type = "list"
	description = "The list of CIDRs for Yellow App Subnets"
}

variable "green_db_subnets" {
	type = "list"
	description = "The list of CIDRs for Green DB Subnets "
}
