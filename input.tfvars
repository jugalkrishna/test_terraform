name= "teststack"
region="us-west-2"
business_unit="Western Union"
cidr="10.229.0.0/16"
red_public_subnets=["10.229.0.0/21","10.229.8.0/21",]
yellow_app_subnets=["10.229.16.0/21","10.229.24.0/21",]
green_db_subnets=["10.229.32.0/21","10.229.40.0/21",]