provider "aws" {
  region = "${var.region}"
  assume_role {
    role_arn     = "arn:aws:iam::799637762685:role/Test_EC2_Access"
  }
}