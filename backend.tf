terraform {
  backend "s3" {
    bucket = "wanv-alg-4-as3-001"
    key    = "terraform/dev/terraform_dev.tfstate"
    region = "us-west-2"
	dynamodb_table = "wanv-alg-4-ddb-001"
	encrypt = true
  }
}